# freeipa_client

Join [FreeIPA](http://www.freeipa.org) domain - very simple

## Requirements

EL6/7

## Role Variables

   freeipa_client_hostname: (undefined)
   freeipa_client_ip_address: (undefined)
   freeipa_client_all_ip_addresses: false
   freeipa_client_domain: (undefined)
   freeipa_client_server: (undefiend)
   freeipa_client_realm: (undefined)
   freeipa_client_principal: (undefined)
   freeipa_client_password: (undefined)
   freeipa_client_ntp: (undefined)
   freeipa_client_ntp_servers: []
   freeipa_client_force_join: false


## Example Playbook

    - hosts: servers
      roles:
         - role: hephyvienna.freeipa-client
           vars:
             freeipa_client_domain: demo1.freeipa.org
             freeipa_client_principal: admin
             freeipa_client_password: Secret123
             freeipa_client_force_join: true

## License

MIT

## Author Information

Written by [Dietrich Liko](Dietrich.Liko@oeaw.ac.at) in April 2019

[Institute for High Energy Physics](http://www.hephy.at) of the
[Austrian Academy of Sciences](http://www.oeaw.ac.at)
